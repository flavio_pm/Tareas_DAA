#Lo primero que se me ocurrió más allá de la fuerza bruta de probar número por número es trabajar
#con listas ordenadas, si nunca se dijo que debamos trabajar con los tríos en el orden ingresado.
#Entonces, el conjunto de números se separaría en dos listas ordenadas, una de números positivos
#y una de números negativos, ambas ordenadas de menor a mayor. Entonces, este programa debería
#almacenar e imprimir los tríos una vez listas.

def insord(lista,objeto) :
    if not lista :
        lista.append(objeto)
        return
    ini = 0
    fin = len(lista) - 1
    while ini <= fin :
        mid = (fin + ini) // 2
        if lista[mid] > objeto :
            if mid == 0 or lista[mid - 1] < objeto :
                lista.insert(mid,objeto)
                break
            else :
                fin = mid - 1
        else :
            if mid == len(lista) - 1 :
                lista.append(objeto)
                break
            ini = mid + 1

#Ingreso de números
listaaux = input("Ingrese la lista de números en la cuál buscar sumas triples a 0: ")
#Agrupación de números
listapos = []
listaneg = []
contcero = 0
while True :
    hallar = listaaux.find(' ')
    if hallar == -1 :
        numero = int(float(listaaux))
    else :
        numero = int(float(listaaux[:hallar]))
    if numero < 0 :
        insord(listaneg, numero)
    elif numero > 0 :
        insord(listapos, numero)
    else :
        contcero += 1
    if hallar == -1 :
        break
    listaaux = listaaux[(hallar + 1):]
#Comprobación de listas.
print("Listas completas. \n\nLista positiva:", end=" ")
for i in range(len(listapos)) :
    print(listapos[i], end=" ")
print("\nLista negativa:", end=" ")
for i in range(len(listaneg)) :
    print(listaneg[i], end=" ")
print("\nCantidad de ceros: ", contcero )
#Algoritmo de ordenamiento: Se recorre una de las listas lentamente. Para cada elemento de
#esa lista, se compara con el elemento mejor comparable de la lista opuesta (menor y menor o
#mayor y mayor.) Dependiendo del signo del resultado, el tercer elemento se lanza a la lista
#más apropiada de las que quedan (si ya da 0, se inserta la dupla + 0 al arreglo de soluciones
#contcero veces.) Se van ajustando el segundo y tercer valores acorde a los signos obtenidos
#hasta que estos recorran toda la lista. Entonces, se pasa al siguiente valor. El trío de 0s
#se agrega al final 1 sola vez, y sin la combinatoria explícita.
#
#Primero, se decide cuál lista recorrer y se renombran las listas acorde.
if len(listapos) > len(listaneg) :
    listapri = listaneg
    listasec = listapos
    pribool = False                                 #Para recordar cual lista es positiva.
else :
    listapri = listapos
    listasec = listaneg
    pribool = True
resultados = []
if contcero > 2:
    resultados.append([0, 0, 0])
#Caso quiebre: cantidad insuficiente de números
if contcero <3 and (len(listapos) == 0 or len(listaneg) == 0) :
    print("No pueden existir tríos de números en el conjunto que sumen 0.")
    quit()
#Pongamos un cero al final de cada lista. Por qué no. Más sencillo que chequear con cero de otra forma.
if contcero > 0 :
    if pribool :
        listasec.append(0)
        listapri.insert(0,0)
    else :
        listapri.append(0)
        listasec.insert(0,0)
#Ya, filo, se recorre la lista corta.
for i in range(len(listapri)) :
    num1 = listapri[i]                              #Recorrido lento.
    j = len(listasec) - 1                           #j indica el último elemento de esta lista.
    num2 = listasec[j]
    while num1 + num2 == 0 :
        if contcero != 0 :
            resultados.append([num1,num2,0])
        j -= 1
        if j == -1 :
            break
        num2 = listasec[j]
    if num1 + num2 > 0 :
        if pribool :
            k = 0
            num3 = listasec[k]
            numbool = False                         #Para discriminar como recorrer su lista.
        else :
            if i == len(listapri) - 1 :
                break                               #Caso quiebre: fin anticipado de lista corta.
            else :
                k = i + 1
                num3 = listapri[k]
                numbool = True
    elif num1 + num2 < 0 :                          #Lo mismo de antes pero al revés .__.
        if pribool :
            if i == len(listapri) - 1 :
                break
            else :
                k = i + 1
                num3 = listapri[k]
                numbool = True
        else :
            k = 0
            num3 = listasec[k]
            numbool = False
    #Habiendo elegido los tres números, se van acercando los valores de las listas.
    while True :
        suma = num1 + num2 + num3
        if suma == 0 :
            resultados.append([num1,num2,num3])
            if j > 0 :
                j -= 1
                num2 = listasec[j]
            #si j ya llegó a fin de recorrido...
            elif k < len(listapri) - 1 and numbool :
                k += 1
                num3 = listapri[k]
            elif k < j - 1 and not numbool :
                k += 1
                num3 = listasec[k]
            #...y k también, tenemos un quiebre.
            else :
                break
        #Mirad como trato de equilibrar dos variables booleanas, en caso
        #variable, con excepciones, para mover uN PURO ÍNDICE SOBS.
        else :
            #numbool permite que j o k lleguen a fin de la línea, por lo que
            #si j o k llega al fin se quiebra el caso.
            if numbool :
                if suma > 0 :
                    if j > 0 :
                        j -= 1
                        num2 = listasec[j]
                    else :
                        #j fin de la línea. Caso quiebre.
                        break
                else :
                    if k < len(listapri) - 1 :
                        k += 1
                        num3 = listapri[k]
                    else :
                        #k fin de la línea. Caso quiebre :
                        break
            #sin numbool, j y k chocan así que no pueden llegar al fin de
            #línea. Luego, el caso quiebre es su encuentro.
            else :
                if suma > 0 :
                    j -= 1
                    if j == k:
                        break
                    num2 = listasec[j]
                else :
                    k += 1
                    if j == k:
                        break
                    num3 = listasec[k]
print("\nTríos que suman 0:")
for i in range(len(resultados)) :
    print(resultados[i][0], '\t', resultados[i][1], '\t', resultados[i][2])
if len(resultados) == 0 :
    print("No se encontró ninguno.")